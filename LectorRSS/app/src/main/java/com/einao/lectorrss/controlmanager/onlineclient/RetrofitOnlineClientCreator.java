package com.einao.lectorrss.controlmanager.onlineclient;

import android.content.Context;

import com.einao.lectorrss.notification.INotificator;

/**
 * Created by akiana on 15/2/16.
 */
public class RetrofitOnlineClientCreator extends OnlineClientCreator {

    public RetrofitClient factoryMethod(Context context, INotificator notificator) {
        return RetrofitClient.getInstance(context, notificator);
    }
}
