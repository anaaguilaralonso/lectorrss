package com.einao.lectorrss.controlmanager.onlineclient;


import com.einao.lectorrss.utils.ApiConstants;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by akiana on 14/2/16.
 */
public interface IRetrofitService {
    @GET(ApiConstants.HTTP_ENDPOINT)
    Call<ResponseBody> listNews();
}
