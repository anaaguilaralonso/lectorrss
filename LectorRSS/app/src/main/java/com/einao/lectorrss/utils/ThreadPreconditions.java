package com.einao.lectorrss.utils;

import android.os.Looper;

/**
 * Created by akiana on 15/2/16.
 */
public class ThreadPreconditions {
    public static void checkOnMainThread() {
        if (Thread.currentThread() != Looper.getMainLooper().getThread()) {
            throw new IllegalStateException("This method should be called from the Main Thread");
        }
    }
}
