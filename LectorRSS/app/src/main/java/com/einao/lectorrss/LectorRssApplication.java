package com.einao.lectorrss;

import android.app.Application;

import com.einao.lectorrss.controlmanager.ControlManager;
import com.einao.lectorrss.controlmanager.offlineclient.GreenDaoOfflineClientCreator;
import com.einao.lectorrss.controlmanager.onlineclient.RetrofitOnlineClientCreator;

/**
 * Created by akiana on 20/2/16.
 */
public class LectorRssApplication extends Application {


    @Override
    public void onCreate() {
        super.onCreate();
        ControlManager.init(new RetrofitOnlineClientCreator(), new GreenDaoOfflineClientCreator());
        ControlManager.getInstance(this);
    }

}
