package com.einao.lectorrss.controlmanager.offlineclient;

import java.util.List;

/**
 * Created by akiana on 20/2/16.
 */
public interface IOfflineClient<T> {

    void add(T clazz);
    List<T> getAll();
}
