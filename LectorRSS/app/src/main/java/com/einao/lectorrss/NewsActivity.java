package com.einao.lectorrss;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.einao.lectorrss.beans.News;
import com.einao.lectorrss.databinding.ActivityNewsBinding;
import com.einao.lectorrss.utils.ApiConstants;

public class NewsActivity extends AppCompatActivity {

    private News mNews;
    ActivityNewsBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_news);

        Bundle bundle = getIntent().getExtras();
        if (bundle.getSerializable(ApiConstants.EXTRA_NEWS) != null) {
            mNews = (News) bundle.getSerializable(ApiConstants.EXTRA_NEWS);
            mBinding.setNews(mNews);
        }
    }

    public void goToUrl(View view) {
        if (mNews.getLink() != null) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(mNews.getLink()));
            startActivity(browserIntent);
        }
    }
}
