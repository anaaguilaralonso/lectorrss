package com.einao.lectorrss.notification;

/**
 * Created by akiana on 15/2/16.
 */
public interface INotificator {
    void notify(INotification notification);
}
