package com.einao.lectorrss.controlmanager.onlineclient.rssresponse;

import java.util.ArrayList;

/**
 * Created by akiana on 15/2/16.
 */
public class Entries {
    String title;
    String link;
    String author;
    String publishedDate;
    String contentSnippet;
    String content;
    ArrayList<String> categories;


    public Entries() {
    }

    public Entries(String title, String link, String author, String publishedDate, String contentSnippet, String content, ArrayList<String> categories) {
        this.title = title;
        this.link = link;
        this.author = author;
        this.publishedDate = publishedDate;
        this.contentSnippet = contentSnippet;
        this.content = content;
        this.categories = categories;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate(String publishedDate) {
        this.publishedDate = publishedDate;
    }

    public String getContentSnippet() {
        return contentSnippet;
    }

    public void setContentSnippet(String contentSnippet) {
        this.contentSnippet = contentSnippet;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public ArrayList<String> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<String> categories) {
        this.categories = categories;
    }
}
