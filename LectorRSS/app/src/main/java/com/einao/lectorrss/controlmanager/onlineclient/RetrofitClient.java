package com.einao.lectorrss.controlmanager.onlineclient;

import android.content.Context;

import com.einao.lectorrss.beans.News;
import com.einao.lectorrss.notification.INotificator;
import com.einao.lectorrss.notification.NotificationGetNews;
import com.einao.lectorrss.utils.ApiConstants;
import com.einao.lectorrss.utils.NewsParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by akiana on 15/2/16.
 */
public class RetrofitClient implements IOnlineClient {

    private static RetrofitClient sInstance;
    private static INotificator mNotificator;
    private Retrofit mRetrofit;

    public RetrofitClient(Context context, INotificator notificator) {
        mRetrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.HTTP_BASEURL)
                .build();

    }

    public static RetrofitClient getInstance(Context context, INotificator notificator) {
        mNotificator = notificator;
        if (sInstance == null) {
            synchronized (RetrofitClient.class) {
                if (sInstance == null) {
                    sInstance = new RetrofitClient(context, notificator);
                }
            }
        }
        return sInstance;
    }

    @Override
    public void getNews() {

        IRetrofitService rssService = mRetrofit.create(IRetrofitService.class);
        Call<ResponseBody> responseBodyCall = rssService.listNews();
        responseBodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                List<News> newsList = new ArrayList<News>();
                try {
                    String json = response.body().string();
                    JSONObject jsonObj = null;
                    jsonObj = new JSONObject(json);
                    newsList = NewsParser.parseJsonToNews(jsonObj);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                mNotificator.notify(new NotificationGetNews(newsList, null));
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mNotificator.notify(new NotificationGetNews(null, t.getLocalizedMessage()));
            }
        });
    }


}
