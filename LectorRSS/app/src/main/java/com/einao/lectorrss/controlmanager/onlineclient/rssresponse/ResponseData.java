package com.einao.lectorrss.controlmanager.onlineclient.rssresponse;

/**
 * Created by akiana on 15/2/16.
 */
public class ResponseData {
    Feed feed;

    public ResponseData(Feed feed) {
        this.feed = feed;
    }

    public ResponseData() {
    }

    public Feed getFeed() {
        return feed;
    }

    public void setFeed(Feed feed) {
        this.feed = feed;
    }
}

