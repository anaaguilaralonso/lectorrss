package com.einao.lectorrss.controlmanager.offlineclient;

import android.content.Context;

/**
 * Created by akiana on 20/2/16.
 */
public abstract class OfflineClientCreator {
    public abstract IOfflineClient factoryMethod(Context context);

}
