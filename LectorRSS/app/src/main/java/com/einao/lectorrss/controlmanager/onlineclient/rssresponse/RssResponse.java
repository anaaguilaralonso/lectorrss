package com.einao.lectorrss.controlmanager.onlineclient.rssresponse;

/**
 * Created by akiana on 15/2/16.
 */

public class RssResponse {
    ResponseData responseData;
    String responseDetails;
    int responseStatus;

    public RssResponse(ResponseData responseData, String responseDetails, int responseStatus) {
        this.responseData = responseData;
        this.responseDetails = responseDetails;
        this.responseStatus = responseStatus;
    }

    public RssResponse() {
    }

    public ResponseData getResponseData() {
        return responseData;
    }

    public void setResponseData(ResponseData responseData) {
        this.responseData = responseData;
    }

    public String getResponseDetails() {
        return responseDetails;
    }

    public void setResponseDetails(String responseDetails) {
        this.responseDetails = responseDetails;
    }

    public int getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(int responseStatus) {
        this.responseStatus = responseStatus;
    }
}
