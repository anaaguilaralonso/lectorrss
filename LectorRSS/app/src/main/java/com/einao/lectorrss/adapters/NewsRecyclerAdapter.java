package com.einao.lectorrss.adapters;

/**
 * Created by akiana on 12/2/16.
 */

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.einao.lectorrss.BR;
import com.einao.lectorrss.R;
import com.einao.lectorrss.beans.News;

import java.util.List;


public class NewsRecyclerAdapter extends RecyclerView.Adapter<NewsRecyclerAdapter.NewsBindingHolder> {

    private List<News> mListNews;
    private View.OnClickListener mOnClickListener;

    public NewsRecyclerAdapter(List<News> listNews, View.OnClickListener onClickListener) {
        mListNews = listNews;
        mOnClickListener = onClickListener;
    }

    @Override
    public NewsBindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_news, parent, false);
        v.setOnClickListener(mOnClickListener);
        NewsBindingHolder holder = new NewsBindingHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(NewsBindingHolder holder, final int position) {
        News news = mListNews.get(position);
        holder.getBinding().setVariable(BR.news, news);
        holder.getBinding().executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mListNews.size();
    }

    public class NewsBindingHolder extends RecyclerView.ViewHolder {

        private ViewDataBinding binding;

        public NewsBindingHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }

        public ViewDataBinding getBinding() {
            return binding;
        }

    }

}
