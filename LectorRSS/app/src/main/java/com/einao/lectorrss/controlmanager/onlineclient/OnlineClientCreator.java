package com.einao.lectorrss.controlmanager.onlineclient;

import android.content.Context;

import com.einao.lectorrss.notification.INotificator;

/**
 * Created by akiana on 15/2/16.
 */
public abstract class OnlineClientCreator {

    public abstract IOnlineClient factoryMethod(Context context, INotificator notificator);
}
