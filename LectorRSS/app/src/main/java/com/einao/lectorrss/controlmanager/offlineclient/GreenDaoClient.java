package com.einao.lectorrss.controlmanager.offlineclient;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.einao.lectorrss.beans.News;
import com.einao.lectorrss.controlmanager.offlineclient.greendaogeneration.DaoMaster;
import com.einao.lectorrss.controlmanager.offlineclient.greendaogeneration.DaoSession;
import com.einao.lectorrss.controlmanager.offlineclient.greendaogeneration.NewsDao;
import com.einao.lectorrss.controlmanager.offlineclient.greendaogeneration.NewsEntity;
import com.einao.lectorrss.utils.ApiConstants;
import com.einao.lectorrss.utils.DateUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by akiana on 20/2/16.
 */
public class GreenDaoClient implements IOfflineClient<News> {

    private static GreenDaoClient sInstance;

    private Context mContext;
    private NewsDao mNewsDao;

    public GreenDaoClient(Context context) {
        mContext = context;
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(mContext, ApiConstants.DATABASE_NAME, null);
        SQLiteDatabase db = helper.getWritableDatabase();

        DaoMaster daoMaster = new DaoMaster(db);
        DaoSession daoSession = daoMaster.newSession();
        mNewsDao = daoSession.getNewsDao();
    }

    public static GreenDaoClient getInstance(Context context) {
        if (sInstance == null) {
            synchronized (GreenDaoClient.class) {
                if (sInstance == null) {
                    sInstance = new GreenDaoClient(context);
                }
            }
        }
        return sInstance;
    }

    @Override
    public void add(News news) {
        List<NewsEntity> newsEntityList = mNewsDao.queryBuilder().where(NewsDao.Properties.Title.like("%" + news.getTitle() + "%")).list();
        if (newsEntityList != null && newsEntityList.size() == 0) {
            NewsEntity newsEntity = new NewsEntity(null, news.getTitle(), news.getLink(), news.getPubDate(), DateUtils.stringToDate(news.getPubDate()), news.getAuthor(), news.getDescription(), news.getImageUrl());
            mNewsDao.insertOrReplace(newsEntity);
        }
    }

    @Override
    public List<News> getAll() {
        List<NewsEntity> newsEntityList = mNewsDao.queryBuilder().orderDesc(NewsDao.Properties.Date).list();
        List<News> newsList = new ArrayList<>();
        News.Builder builder;
        NewsEntity newsEntity;
        for (int i = 0; i < newsEntityList.size(); i++) {
            newsEntity = newsEntityList.get(i);
            builder = new News.Builder(newsEntity.getTitle(), newsEntity.getLink(), newsEntity.getPubDate())
                    .setAuthor(newsEntity.getAuthor())
                    .setDescription(newsEntity.getDescription());
            if (newsEntity.getImageUrl() != null) builder.setImageUrl(newsEntity.getImageUrl());

            newsList.add(builder.build());
        }
        return newsList;
    }

}
