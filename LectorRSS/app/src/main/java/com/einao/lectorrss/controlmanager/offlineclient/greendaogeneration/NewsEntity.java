package com.einao.lectorrss.controlmanager.offlineclient.greendaogeneration;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit. 

/**
 * Entity mapped to table "NEWS".
 */
public class NewsEntity {

    private Long id;
    /**
     * Not-null value.
     */
    private String title;
    /**
     * Not-null value.
     */
    private String link;
    /**
     * Not-null value.
     */
    private String pubDate;
    /**
     * Not-null value.
     */
    private java.util.Date date;
    private String author;
    private String description;
    private String imageUrl;

    public NewsEntity() {
    }

    public NewsEntity(Long id) {
        this.id = id;
    }

    public NewsEntity(Long id, String title, String link, String pubDate, java.util.Date date, String author, String description, String imageUrl) {
        this.id = id;
        this.title = title;
        this.link = link;
        this.pubDate = pubDate;
        this.date = date;
        this.author = author;
        this.description = description;
        this.imageUrl = imageUrl;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Not-null value.
     */
    public String getTitle() {
        return title;
    }

    /**
     * Not-null value; ensure this value is available before it is saved to the database.
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Not-null value.
     */
    public String getLink() {
        return link;
    }

    /**
     * Not-null value; ensure this value is available before it is saved to the database.
     */
    public void setLink(String link) {
        this.link = link;
    }

    /**
     * Not-null value.
     */
    public String getPubDate() {
        return pubDate;
    }

    /**
     * Not-null value; ensure this value is available before it is saved to the database.
     */
    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    /**
     * Not-null value.
     */
    public java.util.Date getDate() {
        return date;
    }

    /**
     * Not-null value; ensure this value is available before it is saved to the database.
     */
    public void setDate(java.util.Date date) {
        this.date = date;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

}
