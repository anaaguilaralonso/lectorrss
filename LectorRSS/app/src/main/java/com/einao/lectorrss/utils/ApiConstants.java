package com.einao.lectorrss.utils;

/**
 * Created by akiana on 12/2/16.
 */
public class ApiConstants {

    public static final String EXTRA_NEWS = "news";
    public static final String DATABASE_NAME = "news-db";
    public static final String HTTP_BASEURL = "https://ajax.googleapis.com/ajax/services/";
    public static final String HTTP_ENDPOINT = "feed/load?v=1.0&q=http://www.abc.es/rss/feeds/abc_ultima.xml";


}
