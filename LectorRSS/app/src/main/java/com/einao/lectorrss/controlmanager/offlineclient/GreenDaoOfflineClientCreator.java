package com.einao.lectorrss.controlmanager.offlineclient;

import android.content.Context;

/**
 * Created by akiana on 20/2/16.
 */
public class GreenDaoOfflineClientCreator extends OfflineClientCreator {
    public GreenDaoClient factoryMethod(Context context) {
        return GreenDaoClient.getInstance(context);
    }
}
