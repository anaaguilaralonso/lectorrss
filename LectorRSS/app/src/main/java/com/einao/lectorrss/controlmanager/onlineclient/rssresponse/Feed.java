package com.einao.lectorrss.controlmanager.onlineclient.rssresponse;

import java.util.ArrayList;

/**
 * Created by akiana on 15/2/16.
 */
public class Feed {

    String feedUrl;
    String title;
    String link;
    String author;
    String description;
    String type;
    ArrayList<Entries> entries;

    public Feed(String feedUrl, String title, String link, String author, String description, String type, ArrayList<Entries> entries) {
        this.feedUrl = feedUrl;
        this.title = title;
        this.link = link;
        this.author = author;
        this.description = description;
        this.type = type;
        this.entries = entries;
    }

    public Feed() {
    }

    public String getFeedUrl() {
        return feedUrl;
    }

    public void setFeedUrl(String feedUrl) {
        this.feedUrl = feedUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<Entries> getEntries() {
        return entries;
    }

    public void setEntries(ArrayList<Entries> entries) {
        this.entries = entries;
    }
}
