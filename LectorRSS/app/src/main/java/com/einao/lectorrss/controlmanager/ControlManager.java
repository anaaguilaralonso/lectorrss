package com.einao.lectorrss.controlmanager;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import com.einao.lectorrss.beans.News;
import com.einao.lectorrss.controlmanager.offlineclient.IOfflineClient;
import com.einao.lectorrss.controlmanager.offlineclient.OfflineClientCreator;
import com.einao.lectorrss.controlmanager.onlineclient.IOnlineClient;
import com.einao.lectorrss.controlmanager.onlineclient.OnlineClientCreator;
import com.einao.lectorrss.notification.INotification;
import com.einao.lectorrss.notification.INotificationListener;
import com.einao.lectorrss.notification.INotificator;
import com.einao.lectorrss.notification.NotificationGetNews;
import com.einao.lectorrss.notification.NotificationNewNews;
import com.einao.lectorrss.utils.ThreadPreconditions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by akiana on 15/2/16.
 */
public class ControlManager {

    private static ControlManager sInstance = null;
    private static OnlineClientCreator sOnlineClientCreator = null;
    private static OfflineClientCreator sOfflineClientCreator = null;
    private IOnlineClient mOnlineClient;
    private IOfflineClient mOfflineClient;
    private INotificator mNotificator;
    private volatile Handler mHandler;

    private final Object mNotificationListenersLock = new Object();
    private final Map<Class<?>, List<INotificationListener>> mNotificationListeners;

    private ControlManager(Context context) {
        mHandler = new Handler(Looper.getMainLooper());
        mNotificationListeners = new HashMap<>();
        mNotificator = new INotificator() {
            @Override
            public void notify(INotification notification) {
                if (notification != null) {
                    fireNotification(notification);
                }
            }
        };

        mOnlineClient = sOnlineClientCreator.factoryMethod(context, mNotificator);
        mOfflineClient = sOfflineClientCreator.factoryMethod(context);
    }

    public static void init(OnlineClientCreator onlineClientCreator, OfflineClientCreator offlineClientCreator) {
        sOnlineClientCreator = onlineClientCreator;
        sOfflineClientCreator = offlineClientCreator;
    }

    public static ControlManager getInstance(Context context) {
        if (sInstance == null && sOnlineClientCreator != null && sOfflineClientCreator != null) {
            synchronized (ControlManager.class) {
                if (sInstance == null) {
                    sInstance = new ControlManager(context);
                }
            }
        }
        return sInstance;
    }

    public List<News> getNews() {
        // get offline news
        List<News> newsList = new ArrayList<News>();
        List offlineNews = mOfflineClient.getAll();
        for (int i = 0; i < offlineNews.size(); i++) {
            newsList.add((News) offlineNews.get(i));
        }
        // get online news
        addNotificationListener(mNotificationListenerNews, NotificationGetNews.class);
        mOnlineClient.getNews();
        return newsList;
    }

    private void fireNotification(final INotification notification) {
        // Always notify in UI thread
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                dispatchNotificationToListeners(notification);
            }
        });
    }

    private void dispatchNotificationToListeners(INotification notification) {
        ThreadPreconditions.checkOnMainThread();
        synchronized (mNotificationListenersLock) {
            List<INotificationListener> listeners = mNotificationListeners.get(notification.getClass());
            if (listeners != null) {
                for (INotificationListener listener : listeners) {
                    listener.onNotification(notification);
                }
            }
        }
    }

    public void addNotificationListener(INotificationListener listener,
                                        Class<?> notificationClass) {

        synchronized (mNotificationListenersLock) {
            List<INotificationListener> listeners = mNotificationListeners.get(notificationClass);
            if (listeners == null) {
                listeners = new ArrayList<>();
            }

            if (!listeners.contains(listener)) {
                listeners.add(listener);
                mNotificationListeners.put(notificationClass, listeners);
            }
        }
    }

    public void removeNotificationListener(INotificationListener listener,
                                           Class<?> notificationClass) {
        synchronized (mNotificationListenersLock) {
            List<INotificationListener> listeners = mNotificationListeners.get(notificationClass);
            if (listeners != null) {
                listeners.remove(listener);
                if (listeners.size() == 0) {
                    mNotificationListeners.remove(notificationClass);
                } else {
                    mNotificationListeners.put(notificationClass, listeners);
                }
            }
        }
    }

    private INotificationListener mNotificationListenerNews = new INotificationListener() {
        @Override
        public void onNotification(INotification notification) {
            NotificationGetNews n = (NotificationGetNews) notification;
            if (n.getErrorMessage() != null) {
                mNotificator.notify(new NotificationNewNews(null, n.getErrorMessage()));
                return;
            }
            // store new data on database
            List<News> onlineNewsList = n.getList();
            if (onlineNewsList != null) {
                for (int i = 0; i < onlineNewsList.size(); i++) {
                    mOfflineClient.add(onlineNewsList.get(i));
                }
                // get all data from database
                removeNotificationListener(mNotificationListenerNews, NotificationGetNews.class);
                List<News> news = mOfflineClient.getAll();
                mNotificator.notify(new NotificationNewNews(news, null));
            } else {
                removeNotificationListener(mNotificationListenerNews, NotificationGetNews.class);
                mNotificator.notify(new NotificationNewNews(null, null));
            }
        }
    };

}
