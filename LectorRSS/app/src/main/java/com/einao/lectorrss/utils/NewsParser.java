package com.einao.lectorrss.utils;

import com.einao.lectorrss.beans.News;
import com.einao.lectorrss.controlmanager.onlineclient.rssresponse.Entries;
import com.einao.lectorrss.controlmanager.onlineclient.rssresponse.RssResponse;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by akiana on 22/2/16.
 */
public class NewsParser {

    public static List<News> parseJsonToNews(JSONObject json) {

        List<News> newsList = new ArrayList<News>();

        Gson gson = new Gson();
        RssResponse res = gson.fromJson(json.toString(), RssResponse.class);

        ArrayList<Entries> entriesList = res.getResponseData().getFeed().getEntries();
        String image = "";
        String content = "";
        for (int i = 0; i < entriesList.size(); i++) {
            Entries entries = entriesList.get(i);
            content = entries.getContent();
            Matcher m = Pattern.compile("src\\s*=\\s*\"(.+?)\"").matcher(content);
            if (m.find()) {
                image = m.group();
                image = image.substring(image.indexOf("=") + 2, image.length() - 1);
            }
            m = Pattern.compile(">(.*?)<").matcher(content);
            if (m.find()) {
                content = m.group().compareTo("><") == 0 ? content.substring(0, content.indexOf("<")) : m.group().substring(1, m.group().length() - 1);
            }
            News.Builder builder = new News.Builder(entries.getTitle(), entries.getLink(), entries.getPublishedDate())
                    .setAuthor(entries.getAuthor())
                    .setDescription(content);
            if (image != null && !image.isEmpty()) {
                builder.setImageUrl(image);
            }

            News news = builder.build();
            newsList.add(news);
            image = "";
        }

        return newsList;
    }
}
