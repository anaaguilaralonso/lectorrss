package com.einao.lectorrss.beans;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.einao.lectorrss.R;
import com.squareup.picasso.Picasso;

import java.io.Serializable;

/**
 * Created by akiana on 12/2/16.
 */
public class News implements Serializable {

    private String title;
    private String link;
    private String pubDate;
    private String author;
    private String description;
    private String imageUrl;


    public News(String title, String link, String pubDate) {
        this.title = title;
        this.link = link;
        this.pubDate = pubDate;
    }

    public News(Builder builder) {
        this.title = builder.title;
        this.link = builder.link;
        this.pubDate = builder.pubDate;
        this.author = builder.author;
        this.description = builder.description;
        this.imageUrl = builder.imageUrl;
    }

    public String getTitle() {
        return title;
    }

    public String getLink() {
        return link;
    }

    public String getPubDate() {
        return pubDate;
    }

    public String getAuthor() {
        return author;
    }

    public String getDescription() {
        return description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    @BindingAdapter({"bind:imageUrl"})
    public static void loadImage(ImageView view, String imageUrl) {
        Picasso.with(view.getContext())
                .load(imageUrl)
                .placeholder(R.mipmap.ic_image)
                .into(view);
    }

    public static class Builder {
        private String title;
        private String link;
        private String pubDate;
        private String author;
        private String description;
        private String imageUrl;

        public Builder(String title, String link, String pubDate) {
            this.title = title;
            this.link = link;
            this.pubDate = pubDate;
        }

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder setLink(String link) {
            this.link = link;
            return this;
        }

        public Builder setPubDate(String pubDate) {
            this.pubDate = pubDate;
            return this;
        }

        public Builder setAuthor(String author) {
            this.author = author;
            return this;
        }

        public Builder setDescription(String description) {
            this.description = description;
            return this;
        }

        public Builder setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
            return this;
        }

        public News build() {
            if (title == null) {
                throw new IllegalArgumentException("Title cannot be null");
            }
            if (link == null) {
                throw new IllegalArgumentException("Link cannot be null");
            }
            if (pubDate == null) {
                throw new IllegalArgumentException("PubDate cannot be null");
            }
            return new News(this);
        }


    }


}
