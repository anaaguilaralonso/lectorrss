package com.einao.lectorrss.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by akiana on 22/2/16.
 */
public class DateUtils {

    public static Date stringToDate(String aDate) {
        SimpleDateFormat format = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z");
        Date date = null;
        try {
            date = format.parse(aDate);
        } catch (ParseException e) {
            return null;
        }
        return date;
    }
}
