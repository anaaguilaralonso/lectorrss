package com.einao.lectorrss;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.einao.lectorrss.adapters.NewsRecyclerAdapter;
import com.einao.lectorrss.beans.News;
import com.einao.lectorrss.controlmanager.ControlManager;
import com.einao.lectorrss.notification.INotification;
import com.einao.lectorrss.notification.INotificationListener;
import com.einao.lectorrss.notification.NotificationGetNews;
import com.einao.lectorrss.notification.NotificationNewNews;
import com.einao.lectorrss.utils.ApiConstants;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    ArrayList<News> mList;

    Context mContext;
    ControlManager mControlManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mContext = this;

        mRecyclerView = (RecyclerView) findViewById(R.id.rVNews);

        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mList = new ArrayList<News>();

        mAdapter = new NewsRecyclerAdapter(mList, new RecyclerOnClickListener());
        mRecyclerView.setAdapter(mAdapter);

        mControlManager = ControlManager.getInstance(this);
        if (mControlManager != null) {
            mControlManager.addNotificationListener(mNotificationListenerNewNews, NotificationNewNews.class);
            mList.addAll(mControlManager.getNews());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mControlManager.removeNotificationListener(mNotificationListenerNewNews, NotificationGetNews.class);
    }

    public class RecyclerOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            int itemPosition = mRecyclerView.getChildPosition(v);
            Intent intent = new Intent(MainActivity.this, NewsActivity.class);
            intent.putExtra(ApiConstants.EXTRA_NEWS, mList.get(itemPosition));
            startActivity(intent);

        }
    }

    private INotificationListener mNotificationListenerNewNews = new INotificationListener() {
        @Override
        public void onNotification(INotification notification) {
            NotificationNewNews n = (NotificationNewNews) notification;
            if (n.getErrorMessage() != null) {
                Toast.makeText(mContext, "Error: " + n.getErrorMessage(), Toast.LENGTH_LONG).show();
                return;
            }
            if (n.getList() != null) {
                mList.clear();
                for (int i = 0; i < n.getList().size(); i++) {
                    mList.add(n.getList().get(i));
                }
                mAdapter.notifyDataSetChanged();
            }
        }
    };
}
