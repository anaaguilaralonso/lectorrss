package com.einao.lectorrss.notification;

import com.einao.lectorrss.beans.News;

import java.util.List;

/**
 * Created by akiana on 15/2/16.
 */
public class NotificationGetNews implements INotification {

    private List<News> mList;
    private String mErrorMessage;

    public NotificationGetNews(List<News> list, String localizedMessage) {
        mList = list;
        mErrorMessage = localizedMessage;
    }

    public String getErrorMessage() {
        return mErrorMessage;
    }

    public List<News> getList() {
        return mList;
    }

}
