DESCRIPCIÓN DE LA SOLUCIÓN

ESQUEMA SIMPLIFICADO DE LA ARQUITECTURA

[lectorrss.png]

PATRONES

- Observer Pattern
Se usa el patrón observer para notificar cuándo ha concluido una petición al servidor. El patrón observer utiliza mensajes o eventos para informar a los objetos que requieren ser notificados de los cambios que ocurren en un objeto concreto. En el caso del LectorRSS, se utiliza con las llamadas al servidor. En primer lugar desde el MainActivity se realiza una petición al ControlManager (getNews), a la que el ControlManager responde con las noticias almacenadas en base de datos. Además se registra un listener para que el activity sea notificado de si hay nuevas noticias disponibles. Por su lado, el ControlManager realiza la petición de las noticias almacenadas en la base de datos y se las envía al activity. Además, realiza una petición de las nuevas noticias y registra un listener para atender esas noticias. Cuando la petición haya terminado, se notifica a los suscriptores, de modo que el OnlineClient notifica al listener del ControlManager que la petición finalizó. El ControlManager almacena las nuevas noticias en la base de datos y notifica al listener de la activity mandándole la lista actualizada de noticias.
Se ha creado el sistema de eventos y no se ha utilizado Otto debido al corto alcance del proyecto, adecuándolo más a la finalidad del mismo.

- Factory
Se ha utilizado el patrón Factory en la creación de los clientes online (Retrofit) y offline (GreenDao), para desacoplar el código permitiendo una injección de dependecias, por lo que el Controlmanager sólo conoce interfaces de IOnlineClient e IOfflineClient y utiliza sus métodos, pero la instancia específica se declara en el Application. De esta manera se diseñan los componentes de una manera más modular e independiente de las librerías utilizada, se desacopla el código. Así, si se debe eliminar o sustituir alguna de las librerías elegidas, se exponga a cambios el menor número de componentes de la aplicación, favoreciendo los cambios introducidos en la aplicación.

- Builder
Para la abstracción del proceso de creación de un objeto complejo, en concreto del objeto News. En la creación se evaluará que algunos parámetros no puedan ser nulos.

- Singleton
Se utiliza para asegurar la creación de un único objeto. En este caso se utiliza para el ControlManager, el RetrofitClient y el GreenDaoClient, que se crean para que se utilice el mismo objeto a lo largo de toda la aplicación.

LIBRERIAS

- Retrofit
Cliente REST para realización de peticiones HTTP.

- GreenDao
ORM para gestión de base de datos.

- Picasso
Permite la carga de imágenes en la aplicación de manera efectiva, eliminando errores derivados de la cancelación de descarga o administrando correctamente los recursos con el caching automático de memoria y disco.

- Gson
Permite serializar/deserializar datos de json a objeto java y viceversa.



